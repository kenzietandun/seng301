# SENG301 Assignment 1 (2020) - student answers

## Architecture decisions

Please discuss the architecture followed by the template project you received and 
the reasons why you changed it or not.


## Task 1 - U1-AC6

Briefly explain where you modified the code (which method/s) and what modifications 
you applied using the form:
- `service.IngredientService.addIngredient` update behaviour to...


## Task 2 - U2

List the tasks you created to implement this story. For each task, we expect you to 
follow the structure below (*this is an example, we expect you to update/discard it*):

### Task 1 - create stubs for methods with empty cucumber tests 

- Estimation (in hours) : 1
- Time spent (in hours) : 0.75
- Description: Write the overall scenario as a cucumber feature and create all method 
stubs from front to back end to add a recipe into the database. 


## Task 3 - retrospect

### What went well?

List what you are proud of in terms of process followed.

### What needs to be improved?

List what you struggled with and how you think you can improve that.

### Action items

List up to 3 SMART action items for your future software engineering projects.