/*
 * Copyright (c) 2020. University of Canterbury
 *
 * This file is part of SENG301 lab material.
 *
 *  The lab material is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The lab material is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this material.  If not, see <https://www.gnu.org/licenses/>.
 */

package uc.seng301.asg1.service;

import org.hibernate.SessionFactory;
import uc.seng301.asg1.App;
import uc.seng301.asg1.accessor.IngredientAccessor;
import uc.seng301.asg1.entity.Ingredient;


/**
 * This class manages Ingredients' business logic.
 */
public class IngredientService {

  private final IngredientAccessor accessor;

  /**
   * Default Constructor
   * @param factory the JPA session factory (to be passed to accessor)
   */
  public IngredientService(SessionFactory factory) {
    accessor = new IngredientAccessor(factory);
  }

  /**
   * Deals with the insertion an ingredient by checking the integrity of an ingredient and calling the accessor
   * @param name the ingredient name to be inserted
   * @return the ID of the newly inserted ingredient with given name, -1 if given name is invalid
   */
  public int addIngredient(String name) {
    if (!ingredientNameIsValid(name)) {
      return -1;
    }
    Ingredient ingredient = new Ingredient();
    ingredient.setName(name.trim());
    return accessor.save(ingredient);
  }

  /**
   * Checks whether given name is valid for an ingredient
   * @param name an ingredient name to be checked
   * @return true if given name contains only letters or white space
   */
  private boolean ingredientNameIsValid(String name) {
    return null != name && !name.isEmpty()
        && name.chars().allMatch(i -> Character.isAlphabetic(i) || Character.isWhitespace(i));
  }

}
