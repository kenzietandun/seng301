Feature: Recipe

  Scenario: AddNewRecipe
    Given the recipe "Honey Soy Chicken" does not already exist
    When the user inserts a recipe "Honey Soy Chicken"
    Then the recipe "Honey Soy Chicken" is saved in the database

  Scenario: AddNewRecipeWithDifferentCaseShouldSaveBoth
    Given the recipe "Honey Soy Chicken" does not already exist
    When the user inserts a recipe "Honey Soy Chicken"
    And the user inserts a recipe "hONEy sOY cHICKEN"
    Then the recipe "Honey Soy Chicken" is saved in the database
    And the recipe "hONEy sOY cHICKEN" is saved in the database

  Scenario: AddNewRecipeInvalidName
    Given the recipe "M@e Ploy Thailand Curry" does not already exist
    When the user inserts a recipe "M@e Ploy Thailand Curry"
    Then the recipe "M@e Ploy Thailand Curry" is not saved in the database

  Scenario: AddNewRecipeEmptyName
    Given the recipe " " does not already exist
    When the user inserts a recipe " "
    Then the recipe " " is not saved in the database

  Scenario: AddNewRecipeSinglePreparationStep
    Given the recipe "Spaghetti" does not already exist
    When the user inserts a recipe "Spaghetti"
    And add a preparation step "Boil the noodles" with ingredient "noodles" to recipe "Spaghetti"
    Then the recipe "Spaghetti" is saved in the database
    And recipe "Spaghetti" has preparation step 1 as "Boil the noodles"
    And the preparation step has ingredient "noodles"

  Scenario: AddNewRecipeMultiplePreparationStep
    Given the recipe "Spaghetti" does not already exist
    When the user inserts a recipe "Spaghetti"
    And add a preparation step "Boil the noodles" with ingredient "noodles" to recipe "Spaghetti"
    And add a preparation step "Make balinese sauce" with ingredient "beef" to recipe "Spaghetti"
    And add a preparation step "Put some cheese on it" with ingredient "gouda" and "edam" to recipe "Spaghetti"
    Then the recipe "Spaghetti" is saved in the database
    And recipe "Spaghetti" has preparation step 3 as "Put some cheese on it"
    And the preparation step has ingredient "gouda" and "edam"

  Scenario: AddNewRecipeMultiplePreparationStep
    Given the recipe "Spaghetti" does not already exist
    When the user inserts a recipe "Spaghetti"
    And add a preparation step "Boil the noodles" with ingredient "noodles" to recipe "Spaghetti"
    And add a preparation step "Make balinese sauce" with ingredient "beef" to recipe "Spaghetti"
    And add a preparation step "Put some cheese on it" with ingredient "gouda" and "GOUDA" to recipe "Spaghetti"
    Then the recipe "Spaghetti" is saved in the database
    And recipe "Spaghetti" has preparation step 3 as "Put some cheese on it"
    And the preparation step has only ingredient "gouda"
