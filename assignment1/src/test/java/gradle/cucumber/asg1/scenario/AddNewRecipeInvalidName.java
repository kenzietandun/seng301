package gradle.cucumber.asg1.scenario;

import io.cucumber.java.Before;
import io.cucumber.java.en.Then;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import uc.seng301.asg1.accessor.RecipeAccessor;
import uc.seng301.asg1.service.RecipeService;

import static org.junit.Assert.assertFalse;

public class AddNewRecipeInvalidName {

    private SessionFactory factory;
    private RecipeService service;
    private RecipeAccessor accessor;

    @Before
    public void setUp() {
        Configuration configuration = new Configuration();
        configuration.configure();

        factory = configuration.buildSessionFactory();
        service = new RecipeService(factory);
        accessor = new RecipeAccessor(factory);
    }

    @Then("the recipe {string} is not saved in the database")
    public void theRecipeIsNotSavedInTheDatabase(String string) {
        assertFalse(accessor.isRecipeExist(string));
    }

}
