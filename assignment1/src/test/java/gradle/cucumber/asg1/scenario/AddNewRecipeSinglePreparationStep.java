package gradle.cucumber.asg1.scenario;

import io.cucumber.java.Before;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import uc.seng301.asg1.accessor.PreparationStepAccessor;
import uc.seng301.asg1.entity.Ingredient;
import uc.seng301.asg1.entity.PreparationStep;
import uc.seng301.asg1.service.IngredientService;
import uc.seng301.asg1.service.PreparationStepService;
import uc.seng301.asg1.service.RecipeService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AddNewRecipeSinglePreparationStep {

    private SessionFactory factory;
    private RecipeService recipeService;
    private IngredientService ingredientService;
    private PreparationStepService preparationStepService;
    private PreparationStepAccessor preparationStepAccessor;


    private PreparationStep prepInDb;

    @Before
    public void setUp() {
        Configuration configuration = new Configuration();
        configuration.configure();

        factory = configuration.buildSessionFactory();
        recipeService = new RecipeService(factory);
        ingredientService = new IngredientService(factory);
        preparationStepService = new PreparationStepService(factory);
        preparationStepAccessor = new PreparationStepAccessor(factory);
    }

    @When("add a preparation step {string} with ingredient {string} to recipe {string}")
    public void addAPreparationStepWithIngredient(String stepDesc, String ingredientName, String recipeName) {
        Short recipeId = recipeService.getByName(recipeName).get(0);

        PreparationStep p = new PreparationStep();
        p.setDescription(stepDesc);

        int latestPrepStep = preparationStepAccessor.getLatestStepNumberByRecipeId(recipeId);
        p.setStepNumber((short) ++latestPrepStep);

        int ingredientId = ingredientService.addOrIgnoreIngredient(ingredientName);
        preparationStepService.addPreparationStep(recipeId, stepDesc, new ArrayList<>(Collections.singletonList(ingredientId)));
    }

    @Then("recipe {string} has preparation step {short} as {string}")
    public void recipeHasPreparationStepAs(String recipeName, Short stepNum, String stepDescription) {
        List<Short> recipeIds = recipeService.getByName(recipeName);
        Short recipeId = recipeIds.get(0);
        prepInDb = preparationStepService.getByRecipeId(recipeId).get(stepNum - 1);
        assertEquals(prepInDb.getDescription(), stepDescription);
        assertEquals(prepInDb.getStepNumber(), stepNum);
    }

    @Then("the preparation step has ingredient {string}")
    public void thePreparationStepHasIngredient(String ingredientName) {
        assertEquals(1,
            prepInDb.getIngredients().stream()
                .filter(e -> e.getName().equals(ingredientName))
                .count());
    }

    @When("add a preparation step {string} with ingredient {string} and {string} to recipe {string}")
    public void addAPreparationStepWithIngredientAndToRecipe(String stepDesc, String ing1, String ing2, String recipeName) {
        Short recipeId = recipeService.getByName(recipeName).get(0);
        PreparationStep p = new PreparationStep();
        p.setDescription(stepDesc);

        int latestPrepStep = preparationStepAccessor.getLatestStepNumberByRecipeId(recipeId);
        p.setStepNumber((short) ++latestPrepStep);


        List<Integer> ingredientIds = new ArrayList<>();
        ingredientIds.add(ingredientService.addOrIgnoreIngredient(ing1));
        ingredientIds.add(ingredientService.addOrIgnoreIngredient(ing2));

        preparationStepService.addPreparationStep(recipeId, stepDesc, ingredientIds);
    }

    @Then("the preparation step has ingredient {string} and {string}")
    public void thePreparationStepHasIngredientAnd(String ing1, String ing2) {
        Set<Ingredient> ingredients = prepInDb.getIngredients();
        Set<String> ingredientNames = ingredients.stream()
            .map(Ingredient::getName)
            .collect(Collectors.toSet());

        assertTrue(ingredientNames.contains(ing1));
        assertTrue(ingredientNames.contains(ing2));
    }

    @Then("the preparation step has only ingredient {string}")
    public void thePreparationStepHasOnlyIngredient(String ingredientName) {
        Set<Ingredient> ingredients = prepInDb.getIngredients();
        Set<String> ingredientNames = ingredients.stream()
            .map(Ingredient::getName)
            .collect(Collectors.toSet());

        assertEquals(1, ingredientNames.size());
        assertTrue(ingredientNames.contains(ingredientName));
    }

}
