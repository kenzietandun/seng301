package gradle.cucumber.asg1.scenario;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import uc.seng301.asg1.accessor.RecipeAccessor;
import uc.seng301.asg1.service.RecipeService;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AddNewRecipe {

    private SessionFactory factory;
    private RecipeService service;
    private RecipeAccessor accessor;

    @Before
    public void setUp() {
        Configuration configuration = new Configuration();
        configuration.configure();

        factory = configuration.buildSessionFactory();
        service = new RecipeService(factory);
        accessor = new RecipeAccessor(factory);
    }

    @Given("the recipe {string} does not already exist")
    public void theRecipeDoesNotAlreadyExist(String name) {
        assertFalse(accessor.isRecipeExist(name));
    }

    @When("the user inserts a recipe {string}")
    public void theUserInsertsARecipe(String name) {
        service.addRecipe(name);
    }

    @Then("the recipe {string} is saved in the database")
    public void theRecipeIsSavedInTheDatabase(String name) {
        assertTrue(accessor.isRecipeExist(name));
    }


}
