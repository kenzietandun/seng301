/*
 * Copyright (c) 2020. University of Canterbury
 *
 * This file is part of SENG301 lab material.
 *
 *  The lab material is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The lab material is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this material.  If not, see <https://www.gnu.org/licenses/>.
 */

package uc.seng301.asg1.service;

import org.hibernate.SessionFactory;
import uc.seng301.asg1.accessor.IngredientAccessor;
import uc.seng301.asg1.entity.Ingredient;
import uc.seng301.asg1.utils.Util;

import java.util.Arrays;
import java.util.stream.Collectors;

import static uc.seng301.asg1.utils.ReturnCode.INGREDIENT_EXISTS;
import static uc.seng301.asg1.utils.ReturnCode.INGREDIENT_INVALID;
import static uc.seng301.asg1.utils.Util.isNameValid;


/**
 * This class manages Ingredients' business logic.
 */
public class IngredientService {

    private final IngredientAccessor accessor;

    /**
     * Default Constructor
     *
     * @param factory the JPA session factory (to be passed to accessor)
     */
    public IngredientService(SessionFactory factory) {
        accessor = new IngredientAccessor(factory);
    }

    /**
     * Deals with the insertion an ingredient by checking the integrity of an ingredient and calling the accessor
     *
     * @param name the ingredient name to be inserted
     * @return the ID of the newly inserted ingredient with given name,
     * INGREDIENT_INVALID if given name is invalid,
     * INGREDIENT_EXISTS if given name is invalid,
     */
    public int addIngredient(String name) {
        if (!isNameValid(name)) {
            return INGREDIENT_INVALID;
        }

        name = name.trim();
        if (accessor.ingredientExists(name)) {
            return INGREDIENT_EXISTS;
        }

        Ingredient ingredient = new Ingredient();
        ingredient.setName(name);
        return accessor.save(ingredient);
    }

    /**
     * Inserts ingredient into db if it does not exist, if it does ignores it
     * @param name Name of ingredient
     * @return ID of ingredient inserted or found if already exists
     */
    public int addOrIgnoreIngredient(String name) {
        name = name.trim();
        if (accessor.ingredientExists(name)) {
            return accessor.getIngredientByName(name).getIdIngredient();
        }

        Ingredient i = new Ingredient();
        i.setName(name);
        return accessor.save(i);
    }

    /**
     * Given a list of ingredient names separated by commas check if all of them are
     * valid
     * @param names List of names separated by comma
     * @return True if all names are valid, False otherwise
     */
    public boolean isNamesValid(String names) {
        String[] ingredientNames = names.split(",");
        return Arrays.stream(ingredientNames)
            .map(String::trim)
            .allMatch(Util::isNameValid);
    }

}
