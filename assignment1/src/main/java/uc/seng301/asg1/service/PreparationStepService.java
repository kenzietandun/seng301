/*
 * Copyright (c) 2020. University of Canterbury
 *
 * This file is part of SENG301 lab material.
 *
 *  The lab material is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The lab material is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this material.  If not, see <https://www.gnu.org/licenses/>.
 */

package uc.seng301.asg1.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.hibernate.SessionFactory;

import uc.seng301.asg1.accessor.IngredientAccessor;
import uc.seng301.asg1.accessor.PreparationStepAccessor;
import uc.seng301.asg1.entity.Ingredient;
import uc.seng301.asg1.entity.PreparationStep;
import uc.seng301.asg1.entity.Recipe;


/**
 * This class manages PreparationSteps' business logic.
 */
public class PreparationStepService {

    private final PreparationStepAccessor accessor;

    /**
     * Default Constructor
     *
     * @param factory the JPA session factory (to be passed to accessor)
     */
    public PreparationStepService(SessionFactory factory) {
        accessor = new PreparationStepAccessor(factory);
    }

    /**
     * Inserts a preparation step to the DB, associate it with a Recipe and the ingredients it has
     * @param recipeId ID of recipe this preparation step is for
     * @param stepDescription Step description
     * @param ingredientIds ID of ingredients associated with this preparation step
     * @return ID of preparation step inserted
     */
    public int addPreparationStep(int recipeId, String stepDescription, List<Integer> ingredientIds) {
        PreparationStep p = new PreparationStep();

        Recipe r = new Recipe();
        r.setIdRecipe((short) recipeId);
        p.setRecipe(r);

        p.setDescription(stepDescription);

        Set<Ingredient> ingredients = new HashSet<>();
        for (int i : ingredientIds) {
            Ingredient stepIngredient = new Ingredient();
            stepIngredient.setIdIngredient((short) i);
            ingredients.add(stepIngredient);
        }
        p.setIngredients(ingredients);

        short latestStepNumber = accessor.getLatestStepNumberByRecipeId((short) recipeId);
        p.setStepNumber(++latestStepNumber);

        return accessor.save(p);
    }

    /**
     * Get list of preparation steps associated with a recipe
     * @param recipeId ID of recipe
     * @return List of preparation steps
     */
    public List<PreparationStep> getByRecipeId(Short recipeId) {
        return accessor.getByRecipeId(recipeId);
    }

    /**
     * Get a summary of preparation steps for one or more recipes
     * @param recipeIds recipe IDs
     * @return Summary of preparation steps
     */
	public String getSummary(List<Short> recipeIds) {
        StringBuilder summary = new StringBuilder();
        for (Short id : recipeIds) {
            summary.append("Recipe ID: ").append(id).append("\n");
            List<PreparationStep> prepSteps = accessor.getByRecipeId(id);
            summary.append(prepSteps.stream()
                .map(p -> p.getStepNumber() + ". " + p.getDescription())
                .limit(3L)
                .collect(Collectors.joining("\n")));
            summary.append("\n\n");
        }
        return summary.toString();
	}

}
