/*
 * Copyright (c) 2020. University of Canterbury
 *
 * This file is part of SENG301 lab material.
 *
 *  The lab material is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The lab material is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this material.  If not, see <https://www.gnu.org/licenses/>.
 */

package uc.seng301.asg1.service;

import static uc.seng301.asg1.utils.ReturnCode.RECIPE_INVALID;
import static uc.seng301.asg1.utils.Util.isNameValid;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.hibernate.SessionFactory;

import uc.seng301.asg1.accessor.RecipeAccessor;
import uc.seng301.asg1.entity.Recipe;

public class RecipeService {


    private final RecipeAccessor accessor;


    public RecipeService(SessionFactory factory) {
        accessor = new RecipeAccessor(factory);
    }

    /**
     * Inserts a recipe into the DB
     * Checks if it has a valid name, if not returns -1
     * @param recipeName Name of recipe
     * @return ID of recipe if valid, else -1
     */
    public int addRecipe(String recipeName) {
        if (!isNameValid(recipeName)) {
            return RECIPE_INVALID;
        }

        recipeName = recipeName.trim();
        Recipe recipe = new Recipe();
        recipe.setName(recipeName);

        return accessor.save(recipe);
    }

    /**
     * Finds a list of recipeIDs that has the name given
     * @param recipeName Name of recipe
     * @return List of recipe IDs
     */
    public List<Short> getByName(String recipeName) {
        return accessor.getByName(recipeName.trim())
            .stream()
            .map(Recipe::getIdRecipe)
            .collect(Collectors.toList());
    }

    public boolean isRecipeExist(String recipeName) {
        return !getByName(recipeName).isEmpty();
    }
}
