package uc.seng301.asg1.utils;

public class Util {

    /**
     * Checks whether given name is valid for an ingredient or recipe
     *
     * @param name an name to be checked
     * @return true if given name contains only letters or white space
     */
    public static boolean isNameValid(String name) {
        return null != name && !name.isBlank()
            && name.chars().allMatch(i -> Character.isAlphabetic(i) || Character.isWhitespace(i));
    }

}
