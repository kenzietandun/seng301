package uc.seng301.asg1.utils;

public class ReturnCode {
    public static final int INGREDIENT_INVALID = -1;
    public static final int INGREDIENT_EXISTS  = -2;

    public static final int RECIPE_INVALID = -1;
}
