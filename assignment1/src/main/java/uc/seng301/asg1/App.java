
/*
 * Copyright (c) 2020. University of Canterbury
 *
 * This file is part of SENG301 lab material.
 *
 *  The lab material is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The lab material is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this material.  If not, see <https://www.gnu.org/licenses/>.
 */

package uc.seng301.asg1;

import static uc.seng301.asg1.utils.ReturnCode.INGREDIENT_EXISTS;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.metamodel.EntityType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Metamodel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import uc.seng301.asg1.service.IngredientService;
import uc.seng301.asg1.service.PreparationStepService;
import uc.seng301.asg1.service.RecipeService;

/**
 * This class contains the main method of MyRecipe App. It handles
 * <ul>
 * <li>all Command Line Interface (CLI) menu items</li>
 * <li>the management of Java Persistence API (JPA) sessions</li>
 * </ul>
 */
public class App {

    // a JPA session factory is needed to access the actual data repository
    private final SessionFactory ourSessionFactory;

    // the scanner for the CLI
    private final Scanner cli;

    // a logger is useful to print messages in a configurable and flexible way
    // see resources/log4j2.xml configuration file
    private final Logger logger = LogManager.getLogger(App.class.getName());

    /**
     * Default constructor
     */
    public App() {
        Configuration configuration = new Configuration();
        configuration.configure();
        ourSessionFactory = configuration.buildSessionFactory();
        cli = new Scanner(System.in);
    }

    /**
     * Get a JPA session factory (i.e. to create sessions to interact with the
     * database)
     *
     * @return the JPA session factory
     * @see org.hibernate.SessionFactory
     */
    public SessionFactory getSessionFactory() {
        return ourSessionFactory;
    }

    /**
     * Get the command line reader to read input from users
     *
     * @return the Scanner opened on the command line
     */
    public Scanner getScanner() {
        return cli;
    }

    /**
     * Main method, starts the Command Line Interface client for MyRecipe app
     *
     * @param args none expected
     */
    public static void main(final String[] args) {
        App app = new App();
        IngredientService ingredientService = new IngredientService(app.getSessionFactory());
        RecipeService recipeService = new RecipeService(app.getSessionFactory());
        PreparationStepService preparationStepService = new PreparationStepService(app.getSessionFactory());

        System.out.println(app.welcome());

        boolean quit = false;
        while (!quit) {
            System.out.print(app.mainMenu());
            String input = app.getScanner().nextLine();
            int menuItem;
            try {
                menuItem = Integer.parseInt(input);
            } catch (NumberFormatException e) {
                menuItem = -1;
            }
            switch (menuItem) {
                case 1:
                    app.addIngredientMenu(app.getScanner(), ingredientService);
                    break;

                case 2:
                    app.addRecipeMenu(app.getScanner(), recipeService, preparationStepService, ingredientService);
                    break;

                case 9:
                    System.out.println(app.serializeDatabaseContent());
                    break;

                case 0:
                    quit = true;
                    break;

                default:
                    System.out.println("Unknown value entered");
            }
        }
    }

    /*----------------
     * PRIVATE METHODS
     ----------------*/

    /**
     * Create the welcome message for the MyRecipe App
     *
     * @return the App banner
     */
    private String welcome() {
        return "\n######################################################\n\n"
                + "  Welcome to MyRecipe - the ultimate recipe book app\n\n"
                + "######################################################";
    }

    /**
     * Create the main menu of MyRecipe App
     *
     * @return the menu with all top level user options
     */
    private String mainMenu() {
        return "\nWhat do you want to do next?\n" + "\t 1. Add an ingredient\n" + "\t 2. Add a recipe\n"
                + "\t 9. Print database content\n" + "\t 0. Exit\n" + "\n" + "Your Answer: ";
    }

    /**
     * Menu handling the creation of a new ingredient
     *
     * @param scanner the input stream to get user input from
     * @param service the ingredient service layer
     */
    private void addIngredientMenu(Scanner scanner, IngredientService service) {
        System.out.println("Enter a name for your ingredient:");
        String name = scanner.nextLine();
        int ingredientId;

        while ((ingredientId = service.addIngredient(name)) < 0) {
            if (ingredientId == INGREDIENT_EXISTS) {
                System.out.println("Ingredient already exists in the database");
                return;
            }
            System.out
                    .println("Invalid name for ingredient (contains other characters than letters), please try again");
            name = scanner.nextLine();
        }
        System.out.println("Ingredient added with ID:" + ingredientId);
    }

    /**
     * Menu handling the creation of a new recipe
     *
     * @param scanner       the input stream to get user input from
     * @param recipeService the recipe service layer
     */
    private void addRecipeMenu(Scanner scanner, RecipeService recipeService,
            PreparationStepService preparationStepService, IngredientService ingService) {
        System.out.print("Enter a name for your recipe: ");
        String recipeName = scanner.nextLine();

        if (recipeService.isRecipeExist(recipeName)) {
            System.out.println("");
            System.out.println("We have found some recipe(s) sharing similar name '" + recipeName + "':");
            List<Short> recipeIds = recipeService.getByName(recipeName);
            System.out.println("");
            System.out.println(preparationStepService.getSummary(recipeIds));
            System.out.println("Do you want to continue? (y|n):");
            String choice = scanner.nextLine();
            if (!choice.equals("y")) {
                return;
            }
        }

        int recipeId;
        while ((recipeId = recipeService.addRecipe(recipeName)) < 0) {
            System.out.println("Invalid name for recipe (contains other characters than letters), please try again");
            recipeName = scanner.nextLine();
        }

        System.out.println("");
        List<String> preparationSteps = new ArrayList<>();
        String currentPrepStep;
        int currentStepNum = 1;
        System.out.println("Please list the preparation step for this recipe (press enter to finish):");
        while (!(currentPrepStep = addRecipePreparationStep(scanner, currentStepNum++)).equals("")) {
            Set<String> ingredients = addPreparationStepIngredients(scanner, ingService);
            List<Integer> ingredientIds = ingredients.stream()
                .map(ingService::addOrIgnoreIngredient)
                .collect(Collectors.toList());
            preparationSteps.add(currentPrepStep);
            preparationStepService.addPreparationStep(recipeId, currentPrepStep, ingredientIds);
        }

        System.out.println("Recipe is saved with ID: " + recipeId + "\n");
    }

    /**
     * Gets current step description from user
     * @param scanner input stream to get user input
     * @param currentStepNum Current step number to be inputted
     * @return Current step description
     */
    private String addRecipePreparationStep(Scanner scanner, int currentStepNum) {
        System.out.print("Step " + currentStepNum + " > ");
        String stepDescription = scanner.nextLine();

        if (stepDescription.equals("")) {
            return "";
        }

        return stepDescription;
    }

    /**
     * Gets the list of ingredients for the current preparation step
     * @param scanner input stream to get user input
     * @param service ingredient service layer
     * @return Set of ingredient names
     */
    private Set<String> addPreparationStepIngredients(Scanner scanner, IngredientService service) {
        Set<String> ingredients = null;
        System.out.println("Please input the ingredients list for this preparation step, separated by comma:");
        System.out.print("> ");
        String ingredientNames = scanner.nextLine();
        while (!service.isNamesValid(ingredientNames)) {
            System.out
                    .println("Invalid name for ingredients (contains other characters than letters), please try again");
            ingredientNames = scanner.nextLine();
        }

        ingredients = Arrays.stream(ingredientNames.split(",")).map(String::trim).collect(Collectors.toSet());

        return ingredients;
    }

    /**
     * Compile the content of all entities stored in the database into user-friendly
     * String.<br>
     * Highly relies on all Entities's toString() method to be fully user-friendly.
     *
     * @return a serialised version of the content of the database.
     */
    private String serializeDatabaseContent() {
        StringBuilder result = new StringBuilder();
        // by using try-with-resource, any resource will be closed auto-magically at the
        // end of the surrounding try-catch
        // see
        // https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html
        // if a resource is not opened inside a try-with-resource (or is not
        // (Auto)Closeable, remember to
        // close it manually in the finally statement of a try-catch
        try (Session session = ourSessionFactory.openSession()) {
            logger.info("querying all managed entities in database");
            Metamodel metamodel = session.getSessionFactory().getMetamodel();
            for (EntityType<?> entityType : metamodel.getEntities()) {
                String entityName = entityType.getName();
                result.append("Content of ").append(entityName).append("\n");
                Query<Object> query = session.createQuery("from " + entityName, Object.class);
                logger.info("executing HQL query '{}'", query.getQueryString());
                for (Object o : query.list()) {
                    result.append("\t").append(o.toString()).append("\n");
                }
            }

        } catch (HibernateException e) {
            result.append("Couldn't query content because of error").append(e.getLocalizedMessage());
            logger.error(e);
        }
        return result.toString();
    }

}
