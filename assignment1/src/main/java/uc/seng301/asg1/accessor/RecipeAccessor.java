package uc.seng301.asg1.accessor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import uc.seng301.asg1.entity.PreparationStep;
import uc.seng301.asg1.entity.Recipe;

import java.util.List;

public class RecipeAccessor {

    private final SessionFactory sessionFactory;
    private static final Logger logger = LogManager.getLogger(IngredientAccessor.class);

    /**
     * Default Constructor
     *
     * @param sessionFactory the sessionFactory to access the database
     */
    public RecipeAccessor(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public int save(Recipe recipe) {
        if (recipe == null) {
            throw new IllegalArgumentException("Recipe can't be null");
        }

        int result;
        Transaction transaction = null;

        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            result = (Short) session.save(recipe);
            transaction.commit();
        } catch (Exception e) {
            result = -1;
            logger.error("Unable to save recipe", e);
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return result;
    }

    /**
     * Checks if a recipe with with a given name exists in the database
     * @param name Name of the recipe
     * @return True if exists, False otherwise
     */
    public boolean isRecipeExist(String name) {
        boolean result = false;
        try (Session session = sessionFactory.openSession()) {
            result = session.createQuery(
                "SELECT COUNT(r.idRecipe) FROM Recipe r WHERE lower(name) ='" + name.toLowerCase() + "'", Long.class)
                .uniqueResult() > 0;
        } catch (Exception e) {
            logger.error("Unable to retrieve recipe with name " + name, e);
        }

        return result;
    }

    /**
     * Returns a list of recipe with that name
     * @param name Name of the recipe
     * @return List of Recipe with that name
     */
    public List<Recipe> getByName(String name) {
        List<Recipe> result = null;
        try (Session session = sessionFactory.openSession()) {
            result = session.createQuery("" +
                "SELECT r " +
                "FROM Recipe r " +
                "WHERE lower(r.name) = '" + name.toLowerCase() + "'", Recipe.class).getResultList();
        } catch (Exception e) {
            logger.error("Unable to retrieve recipe with name " + name, e);
        }
        return result;
    }

}
