/*
 * Copyright (c) 2020. University of Canterbury
 *
 * This file is part of SENG301 lab material.
 *
 *  The lab material is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The lab material is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this material.  If not, see <https://www.gnu.org/licenses/>.
 */

package uc.seng301.asg1.accessor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import uc.seng301.asg1.entity.PreparationStep;

import java.util.List;

/**
 * This class handles the access to the PreparationStep beans
 */
public class PreparationStepAccessor {

    private final SessionFactory sessionFactory;
    private static final Logger logger = LogManager.getLogger(PreparationStepAccessor.class);

    /**
     * Default Constructor
     *
     * @param sessionFactory the sessionFactory to access the database
     */
    public PreparationStepAccessor(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Save an preparationStep into the database
     *
     * @param preparationStep the preparationStep to save
     * @return the preparationStep ID or -1 if an error occurred
     * @throws IllegalArgumentException if passed argument is null
     */
    public int save(PreparationStep preparationStep) {
        if (null == preparationStep) {
            throw new IllegalArgumentException("PreparationStep can't be null");
        }
        int result;
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            result = (Short) session.save(preparationStep);
            transaction.commit();
            logger.debug("saved {}", preparationStep);
        } catch (Exception e) {
            result = -1;
            logger.error("Unable to save preparationStep", e);
            if (null != transaction) {
                transaction.rollback();
            }
        }
        return result;
    }

    /**
     * Returns the list of preparations steps associated with a Recipe
     * @param recipeId ID of the recipe
     * @return Preparations steps associated with the Recipe
     */
    public List<PreparationStep> getByRecipeId(Short recipeId) {
        List<PreparationStep> result = null;
        try (Session session = sessionFactory.openSession()) {
            result = session.createQuery(
                "SELECT DISTINCT (p) " +
                    "FROM PreparationStep p " +
                    "LEFT JOIN FETCH p.recipe " +
                    "LEFT JOIN FETCH p.ingredients " +
                    "WHERE p.recipe = " + recipeId + " " +
                    "ORDER BY p.idStep",
                PreparationStep.class
            ).getResultList();
        } catch (Exception e) {
            logger.error("Unable to get preparation step by id " + recipeId);
        }
        return result;
    }

    /**
     * Returns the latest number of a preparation step associated with the recipe
     * @param recipeId Id of recipe
     * @return Latest step number
     */
    public short getLatestStepNumberByRecipeId(Short recipeId) {
        short result = 0;
        try (Session session = sessionFactory.openSession()) {
            result = session.createQuery(
                "SELECT MAX(p.stepNumber) " +
                    "FROM PreparationStep p " +
                    "WHERE p.recipe = " + recipeId,
                Short.class
            ).uniqueResult();
        } catch (Exception e) {
            logger.error("Unable to get preparation step by id " + recipeId);
        }
        return result;
    }

}
