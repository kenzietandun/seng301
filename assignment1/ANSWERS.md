# SENG301 Assignment 1 (2020) - student answers

## Architecture decisions

Since this is a small sized application I decided to keep this architecture.

I have also taken the liberty to use the returned object ID
as an indicator of the status code when saving object to the database (e.g. ID of -2 means 
ingredient already exists in the database). The proper way of doing this might involve
throwing exceptions from the service level and catching it in the front end.

## Task 1 - U1-AC6

- `service.IngredientService.addIngredient` updated behaviour to return a predefined
  set of return code when an unexpected/invalid ingredient name is provided
  
- `utils.ReturnCodes` created a java class that contain the return codes that will
  be used as a reference throughout the application
  
- `App.addIngredientMenu` added check if duplicate ingredient name is entered, the
  program would return right back to the main menu
  
- `test`/`gradle.cucumber.asg1.scenario.AddAlreadyExistingIngredient` added a test
  to ensure a duplicate ingredient is not added to the database
  
- `test/resources`/`gradle.cucumber.Ingredient` added scenario to test duplicate
  ingredient entry should not be added to database

## Task 2 - U2

##### Task 1 - create stubs for methods with empty cucumber tests for recipe

- Estimation (in hours): 1
- Time spent (in hours): 0.5
- Description: Write the overall scenario as a cucumber feature and create all method 
stubs from front to back end to add a recipe into the database. 

##### Task 2 - implement method that will be used to save a recipe to the database

- Estimation (in hours): 1
- Time spent (in hours): 0.5
- Description: Use IngredientAccessor as a reference and change as needed.

##### Task 3 - implement recipe saving's business logic

- Estimation (in hours): 1
- Time spent (in hours): 0.5
- Description: Recipe name can be not unique but must follow a certain constraint (AC3).

##### Task 4 - add saving recipe functionality to the CLI interface

- Estimation (in hours): 1
- Time spent (in hours): 0.5
- Description: Implement just enough functionality for user to save a recipe. No error
handling is needed at this stage if there was a duplicate recipe.

##### Task 5 - implement error code when saving recipe with invalid name

- Estimation (in hours): 0.5
- Time spent (in hours): 0.2
- Description: when adding an invalid recipe, it should return a particular error
code in the service level

##### Task 6 - add error handling when saving recipe on CLI interface

- Estimation (in hours): 1.5
- Time spent (in hours): 0.5
- Description: If the recipe name is invalid, the app should ask for a new recipe name. 
If the recipe already exists, it should show the existing recipe's summary.

##### Task 7 - create stubs for methods with empty cucumber tests for preparation steps

- Estimation (in hours): 1
- Time spent (in hours): 0.5
- Description: write just enough placeholders to add cucumber tests to test adding
a preparation step

##### Task 8 - implement service and accessor to save a preparation step

- Estimation (in hours): 2
- Time spent (in hours): 2
- Description: use existing accessor and service files as a reference and implement
the same functionality to save preparation step entities

##### Task 9 - add preparation step related questions when adding recipe in CLI

- Estimation (in hours): 1.5
- Time spent (in hours): 4
- Description: each recipe could have multiple steps, each step should have list of
ingredients. 

## Task 3 - retrospect

### What went well?

The more I break task into smaller pieces, the closer the estimation is compared to the
actual time taken. This can be easily seen from task 1 to 8.

### What needs to be improved?

Task 9 could be broken into smaller pieces/planned better.

### Action items

- Plan out how the application should work on a piece of paper before programming

- Write task description that is quantifiable

- Move all stuff that could be a source of distraction away before we start working
