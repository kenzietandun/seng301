package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SQLiteJDBC {
    Connection connection;

    public Connection createConnection() throws SQLException {
        String url = "jdbc:sqlite:lab1.sqlite";
        connection = DriverManager.getConnection(url);
        System.out.println("\nConnected to " + url + "!");
        return connection;
    }

    public boolean isThereRecipeNamedAs(String name) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("select * from recipe where name = ?");
        statement.setString(1, name);
        statement.closeOnCompletion();
        return statement.executeQuery().next();
    }

    public boolean isThereStepNumberXInRecipeY(int stepNumber, String recipeName) throws SQLException {
        assert stepNumber >= 0 && null != recipeName && !recipeName.isBlank();

        int recipeId = getRecipeId(recipeName);
        PreparedStatement statement = connection
                .prepareStatement("select * from preparation_step where id_recipe = ? and step_number = ?");
        statement.setInt(1, recipeId);
        statement.setInt(2, stepNumber);
        return statement.executeQuery().next();
    }

    public void updatePreparationStepNumberXtoDescriptionY(int stepNumber, String description, int recipeId) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("update preparation_step set description = ? where step_number = ? and id_recipe = ?");
        statement.setString(1, description);
        statement.setInt(2, stepNumber);
        statement.setInt(3, recipeId);
        statement.executeUpdate();
    }

    public String getStepDescriptionFromRecipe(String recipeName, int stepNumber) throws SQLException {
        int recipeId = getRecipeId(recipeName);
        PreparedStatement statement = connection.prepareStatement("select description from preparation_step where id_recipe = ? and step_number = ?");
        statement.setInt(1, recipeId);
        statement.setInt(2, stepNumber);
        return statement.executeQuery().getString(1);
    }

    public int getRecipeId(String recipeName) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("select id_recipe from recipe where name = ?");
        statement.setString(1, recipeName);
        return statement.executeQuery().getInt(1);
    }

}
