package gradle.cucumber;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.junit.Assert;

import database.SQLiteJDBC;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class PreparationStep {

    private SQLiteJDBC sqLiteJDBC;

    private Connection connection;
    
    private int recipeId;

    @Before
    public void setup() {
        sqLiteJDBC = new SQLiteJDBC();
        try {
            connection = sqLiteJDBC.createConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Given("I am connected to the lab1 database")
    public void iAmConnectedToTheLab1Database() {
        Assert.assertNotNull(connection);
    }

    @After
    public void close() throws SQLException {
        connection.close();
    }

    @Given("There is a recipe named {string}")
    public void thereIsARecipeNamed(String string) throws SQLException {
        Assert.assertTrue(sqLiteJDBC.isThereRecipeNamedAs(string));
        recipeId = sqLiteJDBC.getRecipeId(string);
        Assert.assertNotNull(recipeId);
    }

    @Given("There is a step number {int} for the recipe {string}")
    public void thereIsAStepNumberForTheRecipe(Integer int1, String string) throws SQLException {
        Assert.assertTrue(sqLiteJDBC.isThereStepNumberXInRecipeY(int1, string));
    }

    @When("I update step number {int} description to {string}")
    public void iUpdateStepNumberDescriptionTo(Integer int1, String string) throws SQLException {
        sqLiteJDBC.updatePreparationStepNumberXtoDescriptionY(int1, string, recipeId);
    }

    @Then("Next time I retrieve recipe {string} step {int} description, I'll get {string}")
    public void nextTimeIRetrieveRecipeStepDescriptionILlGet(String string, Integer int1, String string2) throws SQLException {
        String newDescription = sqLiteJDBC.getStepDescriptionFromRecipe(string, int1);
        Assert.assertTrue(newDescription.equals(string2));
    }

}
